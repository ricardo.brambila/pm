package br.uniriotec.scbaluguel.model;

import lombok.Data;

@Data
public class CobrancaRequest {
    private int valor;
    private int ciclista;

    public CobrancaRequest(int valor, int ciclista) {
        this.valor = valor;
        this.ciclista = ciclista;
    }
}
