package br.uniriotec.scbaluguel.controller;

import br.uniriotec.scbaluguel.exception.ResourceNotFoundException;
import br.uniriotec.scbaluguel.model.Aluguel;
import br.uniriotec.scbaluguel.service.DevolucaoService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotBlank;

@RestController
@RequestMapping("/devolucao")
public class DevolucaoController {

    @Autowired
    private DevolucaoService devolucaoService;

    @Data
    static class Devolucao {
        @NotBlank(message = "id da tranca é obrigatório")
        private String idTranca;

        @NotBlank(message = "id da bicicleta é obrigatório")
        private String idBicicleta;
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<Aluguel> realizarDevolucao(@RequestBody Devolucao devolucao) throws ResourceNotFoundException {
        Aluguel aluguel = this.devolucaoService.realizarDevolucao(devolucao.getIdTranca(), devolucao.getIdBicicleta());

        return ResponseEntity.ok(aluguel);
    }
}
