package br.uniriotec.scbaluguel.service;

import br.uniriotec.scbaluguel.exception.ResourceNotFoundException;
import br.uniriotec.scbaluguel.model.Aluguel;
import br.uniriotec.scbaluguel.repository.IAluguelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DevolucaoService {
    @Autowired
    private IAluguelRepository aluguelRepository;

    public Aluguel realizarDevolucao(String idTranca, String idBicicleta) throws ResourceNotFoundException {
        Aluguel aluguel = this.aluguelRepository.findByBicicleta(idBicicleta);

        if(aluguel == null) throw new ResourceNotFoundException("Aluguel da bicicleta " + idBicicleta + " não encontrado");

        aluguel.setHoraFim();
        aluguel.setTrancaFim(idTranca);

        //TODO: api para cobrar gasto adicional

        return aluguelRepository.save(aluguel);
    }
}
