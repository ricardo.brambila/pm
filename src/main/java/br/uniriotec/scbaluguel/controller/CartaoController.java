package br.uniriotec.scbaluguel.controller;

import br.uniriotec.scbaluguel.exception.ResourceNotFoundException;
import br.uniriotec.scbaluguel.model.Cartao;
import br.uniriotec.scbaluguel.model.Ciclista;
import br.uniriotec.scbaluguel.repository.ICiclistaRepository;
import br.uniriotec.scbaluguel.service.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cartaoDeCredito")
public class CartaoController {
    @Autowired
    private CartaoService cartaoService;

    @GetMapping("/{idCiclista}")
    @ResponseBody
    public ResponseEntity<Cartao> obterCartao(@PathVariable Long idCiclista) throws ResourceNotFoundException {
        Cartao cartao = cartaoService.obterCartao(idCiclista);

        return ResponseEntity.ok(cartao);
    }

    @PutMapping("/{idCiclista}")
    @ResponseBody
    public ResponseEntity<Cartao> alterarCartao(@PathVariable Long idCiclista, @RequestBody Cartao cartao) throws Exception {
        Cartao cartaoDb = cartaoService.alterarCartao(idCiclista, cartao);
        return ResponseEntity.ok(cartaoDb);
    }
}
