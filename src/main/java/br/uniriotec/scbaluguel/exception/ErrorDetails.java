package br.uniriotec.scbaluguel.exception;

import lombok.Data;

import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class ErrorDetails {
    private Date data;
    private String mensagem;
    private String detalhes;

    public ErrorDetails(Date data, String mensagem, String detalhes) {
        this.data = data;
        this.mensagem = mensagem;
        this.detalhes = detalhes;
    }

    public String getData() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return formatter.format(this.data);
    }
}
