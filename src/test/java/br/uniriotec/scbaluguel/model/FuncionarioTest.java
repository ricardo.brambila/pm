package br.uniriotec.scbaluguel.model;

import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public class FuncionarioTest {
    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        this.validator = factory.getValidator();
    }

    @Test
    public void naoDeveAceitarFuncionario() {
        Funcionario funcionario = new Funcionario();
        funcionario.setIdade(15);
        funcionario.setCpf("1234567980");

        Set<ConstraintViolation<Funcionario>> restricoes = validator.validate(funcionario);
        assertThat(restricoes, hasSize(6));
    }

    @Test
    public void deveAceitarFuncionario() {
        Funcionario funcionario = new Funcionario();
        funcionario.setIdade(25);
        funcionario.setCpf("09901706084");
        funcionario.setNome("Cléber Araújo");
        funcionario.setSenha("12345");
        funcionario.setMatricula("3210");
        funcionario.setFuncao("Analista");

        Set<ConstraintViolation<Funcionario>> restricoes = validator.validate(funcionario);
        assertThat(restricoes, hasSize(0));
    }
}
