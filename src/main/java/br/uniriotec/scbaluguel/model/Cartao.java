package br.uniriotec.scbaluguel.model;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@Entity(name = "cartoes")
public class Cartao {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome_titular")
    @NotBlank(message = "cartao:: nomeTitular é obrigatório")
    private String nomeTitular;

    @Column(name = "numero")
    @NotBlank(message = "cartao:: numero é obrigatório")
    @Size(min = 14, max = 16, message = "cartao:: numero longo ou pequeno demais")
    private String numero;

    @Column(name = "validade")
    @Pattern(regexp = "^\\d{2}/\\d{2}$", message = "cartao:: validade no formato mm/yy")
    @NotBlank(message = "cartao:: validade é obrigatório")
    private String validade;

    @Column(name = "cvv")
    @Pattern(regexp = "^\\d{3}$", message = "cartao:: cvv com 3 dígitos")
    private String cvv;

    public Cartao() { }

    public Cartao(String nomeTitular, String numero, String validade, String cvv) {
        this.nomeTitular = nomeTitular;
        this.numero = numero;
        this.validade = validade;
        this.cvv = cvv;
    }
}
