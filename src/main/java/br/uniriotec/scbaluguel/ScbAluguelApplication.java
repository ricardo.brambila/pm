package br.uniriotec.scbaluguel;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.*;

@Controller
@SpringBootApplication(scanBasePackages = {"br.uniriotec.scbaluguel"})
public class ScbAluguelApplication {
	public static void main(String[] args) {
		try {
			SpringApplication.run(ScbAluguelApplication.class, args);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
