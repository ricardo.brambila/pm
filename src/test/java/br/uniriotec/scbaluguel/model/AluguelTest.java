package br.uniriotec.scbaluguel.model;

import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AluguelTest {

    @Test
    public void deveFormatarData() {
        Calendar instance = Calendar.getInstance();
        instance.set(2021, 11, 15, 5, 30, 10);
        Date time = instance.getTime();

        Aluguel aluguel = new Aluguel();
        aluguel.setHoraInicio(time);
        aluguel.setHoraFim(time);

        assertEquals(aluguel.getHoraInicio(), "15/12/2021 05:30:10");
        assertEquals(aluguel.getHoraFim(), "15/12/2021 05:30:10");
    }
}
