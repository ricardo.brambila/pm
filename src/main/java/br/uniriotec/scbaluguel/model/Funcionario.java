package br.uniriotec.scbaluguel.model;

import lombok.Data;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@Data
@Entity
@Table(name = "funcionarios")
public class Funcionario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "matricula")
    @NotBlank(message = "matricula é obrigatório")
    private String matricula;

    @Column(name = "senha")
    @NotBlank(message = "senha é obrigatório")
    private String senha;

    @Column(name = "nome")
    @NotBlank(message = "nome é obrigatório")
    private String nome;

    @Column(name = "idade")
    @Min(value = 18, message = "precisa ser maior de idade")
    private int idade;

    @Column(name = "funcao")
    @NotBlank(message = "funcao é obrigatório")
    private String funcao;

    @Column(name = "cpf")
    @CPF(message = "cpf inválido")
    private String cpf;

    public Funcionario() { }

    public Funcionario(String matricula, String senha, String nome, int idade, String funcao, String cpf) {
        this.matricula = matricula;
        this.senha = senha;
        this.nome = nome;
        this.idade = idade;
        this.funcao = funcao;
        this.cpf = cpf;
    }
}
