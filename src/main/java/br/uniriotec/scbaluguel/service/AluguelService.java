package br.uniriotec.scbaluguel.service;

import br.uniriotec.scbaluguel.exception.ForbiddenException;
import br.uniriotec.scbaluguel.exception.ResourceNotFoundException;
import br.uniriotec.scbaluguel.model.*;
import br.uniriotec.scbaluguel.repository.IAluguelRepository;
import br.uniriotec.scbaluguel.repository.ICiclistaRepository;
import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AluguelService {
    @Autowired
    private ICiclistaRepository ciclistaRepository;

    @Autowired
    private IAluguelRepository aluguelRepository;

    public Aluguel realizarAluguel(Long ciclistaId, String trancaInicio) throws ResourceNotFoundException, ForbiddenException {
        Ciclista ciclista = ciclistaRepository.findById(ciclistaId).orElseThrow(() -> new ResourceNotFoundException("Ciclista não encontrado com ID :: " + ciclistaId));

        if(!ciclista.isAtivado()) throw new ForbiddenException("Ciclista precisa estar ativo para alugar uma bicicleta");

        final TrancaResponse trancaResponse = Unirest
                .get("https://grupo2equipamento.herokuapp.com/tranca/{trancaInicio}")
                .routeParam("trancaInicio", trancaInicio)
                .asObject(TrancaResponse.class)
                .getBody();

//        if(trancaResponse.getBicicleta() == null) throw  new ResourceNotFoundException("Não há uma bicicleta na tranca solicitada");

        final CobrancaResponse cobrancaResponse = Unirest
                .post("https://aluguel-de-bicicletas.herokuapp.com/cobranca")
                .header("Content-Type", "application/json")
                .body(new CobrancaRequest(2, ciclistaId.intValue()))
                .asObject(CobrancaResponse.class)
                .getBody();

//        if(cobrancaResponse.getCodigo().equals("422")) throw  new ResourceNotFoundException(cobrancaResponse.getMensagem());

        Aluguel aluguel = new Aluguel();
        aluguel.setHoraInicio();
        aluguel.setCiclista(ciclistaId);
        aluguel.setTrancaInicio(trancaInicio);
        aluguel.setBicicleta(trancaResponse.getBicicleta());

        Unirest
                .post("https://aluguel-de-bicicletas.herokuapp.com/enviarEmail")
                .header("Content-Type", "application/json")
                .body(new EnviarEmailRequest(ciclista.getEmail(), "Cobrança inicial de 2 reais feita ao alugar a bicicleta " + trancaResponse.getBicicleta()))
                .asEmpty();

        //TODO: LIBERAR A TRANCA ONDE ESTAVA A BICICLETA
        return aluguelRepository.save(aluguel);
    }
}
