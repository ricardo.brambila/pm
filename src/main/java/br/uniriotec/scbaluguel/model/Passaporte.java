package br.uniriotec.scbaluguel.model;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@Entity(name = "passaportes")
public class Passaporte {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "numero")
    @NotBlank(message = "passaporte:: numero é obrigatório")
    private String numero;

    @Column(name = "validade")
    @Pattern(regexp = "^\\d{2}/\\d{2}$", message = "passaporte:: validade no formato mm/yy")
    @NotBlank(message = "cartao:: validade é obrigatório")
    private String validade;

    @Column(name = "pais")
    @NotBlank(message = "passaporte:: país é obrigatório")
    private String pais;

    public Passaporte() {}
}
