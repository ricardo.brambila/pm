package br.uniriotec.scbaluguel.controller;

import br.uniriotec.scbaluguel.exception.ResourceNotFoundException;
import br.uniriotec.scbaluguel.model.Cartao;
import br.uniriotec.scbaluguel.model.Ciclista;
import br.uniriotec.scbaluguel.repository.ICiclistaRepository;
import br.uniriotec.scbaluguel.service.CiclistaService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ciclista")
public class CiclistaController {

    @Autowired
    private CiclistaService ciclistaService;

    @Data
    static class AdicionarCiclistaBody {
        private Ciclista ciclista;
        private Cartao meioDePagamento;
    }

    @GetMapping
    public ResponseEntity<List<Ciclista>> obterCiclistas() {
        List<Ciclista> ciclistas = this.ciclistaService.obterCiclistas();
        return ResponseEntity.ok(ciclistas);
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<Ciclista> adicionarCiclista(@RequestBody AdicionarCiclistaBody body) throws Exception {
        Ciclista ciclista = ciclistaService.adicionarCiclista(body.getCiclista(), body.getMeioDePagamento());
        return ResponseEntity.ok(ciclista);
    }

    @GetMapping("/{idCiclista}")
    @ResponseBody
    public ResponseEntity<Ciclista> obterCiclista(@PathVariable Long idCiclista) throws ResourceNotFoundException {
        Ciclista ciclista = ciclistaService.obterCiclista(idCiclista);
        return ResponseEntity.ok(ciclista);
    }

    @PutMapping("/{idCiclista}")
    @ResponseBody
    public ResponseEntity<Ciclista> alterarCiclista(@PathVariable Long idCiclista, @RequestBody Ciclista ciclista) throws ResourceNotFoundException {
        Ciclista ciclistaDb = ciclistaService.alterarCiclista(idCiclista, ciclista);
        return ResponseEntity.ok(ciclistaDb);
    }

    @PostMapping("/{idCiclista}/ativar")
    @ResponseBody
    public ResponseEntity<String> ativarCiclista(@PathVariable Long idCiclista) throws ResourceNotFoundException {
        String msg = ciclistaService.ativarCiclista(idCiclista);
        return ResponseEntity.ok(msg);
    }

    @GetMapping("/existeEmail/{email}")
    @ResponseBody
    public ResponseEntity<Boolean> verificaEmail(@PathVariable String email) {
        boolean existeEmail = this.ciclistaService.verificaEmail(email);
        return ResponseEntity.ok(existeEmail);
    }
}
