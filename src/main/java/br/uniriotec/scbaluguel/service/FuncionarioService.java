package br.uniriotec.scbaluguel.service;

import br.uniriotec.scbaluguel.exception.ResourceNotFoundException;
import br.uniriotec.scbaluguel.model.Funcionario;
import br.uniriotec.scbaluguel.repository.IFuncionarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class FuncionarioService {
    @Autowired
    private IFuncionarioRepository funcionarioRepository;

    public List<Funcionario> obterFuncionarios() {
        return this.funcionarioRepository.findAll();
    }

    public Funcionario cadastrarFuncionario(Funcionario funcionario) {
        return this.funcionarioRepository.save(funcionario);
    }

    public Funcionario obterFuncionario(Long idFuncionario) throws ResourceNotFoundException {
        return funcionarioRepository.findById(idFuncionario).orElseThrow(() -> new ResourceNotFoundException("Funcionário não encontrado :: " + idFuncionario));
    }

    public Funcionario editarFuncionario(Long idFuncionario, Funcionario funcionario) throws ResourceNotFoundException {
        Funcionario funcionarioDb = funcionarioRepository.findById(idFuncionario).orElseThrow(() -> new ResourceNotFoundException("Funcionário não encontrado :: " + idFuncionario));

        funcionarioDb.setSenha(funcionario.getSenha());
        funcionarioDb.setNome(funcionario.getNome());
        funcionarioDb.setIdade(funcionario.getIdade());
        funcionarioDb.setFuncao(funcionario.getFuncao());
        funcionarioDb.setCpf(funcionario.getCpf());

        return this.funcionarioRepository.save(funcionarioDb);
    }

    public String removerFuncionario(@PathVariable Long idFuncionario) throws ResourceNotFoundException {
        Funcionario funcionario = funcionarioRepository.findById(idFuncionario).orElseThrow(() -> new ResourceNotFoundException("Funcionário não encontrado :: " + idFuncionario));
        this.funcionarioRepository.deleteById(funcionario.getId());
        return "Dados removidos";
    }
}
