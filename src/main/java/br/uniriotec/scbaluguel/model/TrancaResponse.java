package br.uniriotec.scbaluguel.model;

import lombok.Data;

@Data
public class TrancaResponse {
    private String id;
    private String bicicleta;
    private Integer numero;
    private String localizacao;
    private String anoFabricacao;
    private String modelo;
    private String status;
}
