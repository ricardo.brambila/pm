package br.uniriotec.scbaluguel.model;

import lombok.Data;
import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@Entity
@Table(name = "ciclistas")
public class Ciclista {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome", nullable = false)
    @NotBlank(message = "nome é obrigatório")
    private String nome;

    @Column(name = "nascimento")
    @Pattern(regexp = "^\\d{2}/\\d{2}/\\d{4}$", message = "nascimento no formato dd/mm/yyyy")
    private String nascimento;

    @Column(name = "cpf")
    @CPF
    private String cpf;

    @Column(name = "nacionalidade")
    @NotBlank(message = "nacionalidade é obrigatório")
    private String nacionalidade;

    @Column(name = "email")
    @Pattern(regexp = "^[a-z0-9.]+@[a-z0-9]+\\.[a-z]+(\\.[a-z]+)?$", message = "e-mail inválido")
    private String email;

    @Column(name = "senha")
    @NotBlank(message = "senha é obrigatório")
    private String senha;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "passaporte_id")
    private Passaporte passaporte;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "cartao_id")
    private Cartao cartao;

    @Column(name = "ativado")
    private boolean ativado;

    public Ciclista() { }

    public Ciclista(String nome, String nascimento, String cpf, String nacionalidade, String email, String senha, Passaporte passaporte) {
        this.nome = nome;
        this.nascimento = nascimento;
        this.cpf = cpf;
        this.nacionalidade = nacionalidade;
        this.email = email;
        this.senha = senha;
        this.passaporte = passaporte;
    }
}
