package br.uniriotec.scbaluguel.controller;

import br.uniriotec.scbaluguel.exception.ForbiddenException;
import br.uniriotec.scbaluguel.exception.ResourceNotFoundException;
import br.uniriotec.scbaluguel.model.Aluguel;
import br.uniriotec.scbaluguel.model.Ciclista;
import br.uniriotec.scbaluguel.repository.IAluguelRepository;
import br.uniriotec.scbaluguel.repository.ICiclistaRepository;
import br.uniriotec.scbaluguel.service.AluguelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import lombok.Data;

@RestController
@RequestMapping("/aluguel")
public class AluguelController {
    @Autowired
    private AluguelService aluguelService;

    @Data
    static class AluguelBody {
        private Long ciclista;
        private String trancaInicio;
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<Aluguel> realizarAluguel(@RequestBody AluguelBody body) throws ResourceNotFoundException, ForbiddenException {
        Aluguel aluguel = aluguelService.realizarAluguel(body.getCiclista(), body.getTrancaInicio());

        return ResponseEntity.ok(aluguel);
    }
}
