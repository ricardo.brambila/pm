package br.uniriotec.scbaluguel.service;

import br.uniriotec.scbaluguel.exception.ForbiddenException;
import br.uniriotec.scbaluguel.exception.ResourceNotFoundException;
import br.uniriotec.scbaluguel.model.Cartao;
import br.uniriotec.scbaluguel.model.Ciclista;
import br.uniriotec.scbaluguel.model.CobrancaResponse;
import br.uniriotec.scbaluguel.model.EnviarEmailRequest;
import br.uniriotec.scbaluguel.repository.ICiclistaRepository;
import kong.unirest.Unirest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CiclistaService {
    @Autowired
    private ICiclistaRepository ciclistaRepository;

    public List<Ciclista> obterCiclistas() {
        return this.ciclistaRepository.findAll();
    }

    public Ciclista obterCiclista(Long idCiclista) throws ResourceNotFoundException {
        return ciclistaRepository.findById(idCiclista).orElseThrow(() -> new ResourceNotFoundException("Ciclista não encontrado :: " + idCiclista));
    }

    public Ciclista adicionarCiclista(Ciclista ciclista, Cartao meioDePagamento) throws Exception {
        System.out.print("Validando dados do cartão... ");
        System.out.println(meioDePagamento);

        final CobrancaResponse validaResponse = Unirest
                .post("https://aluguel-de-bicicletas.herokuapp.com/validaCartaoDeCredito")
                .header("Content-Type", "application/json")
                .body(meioDePagamento)
                .asObject(CobrancaResponse.class)
                .getBody();

//        if(validaResponse.getCodigo().equals("422")) throw new Exception(validaResponse.getMensagem());
        ciclista.setCartao(meioDePagamento);

        Unirest
                .post("https://aluguel-de-bicicletas.herokuapp.com/enviarEmail")
                .header("Content-Type", "application/json")
                .body(new EnviarEmailRequest(ciclista.getEmail(), "Usuário cadastrado, ative-o clicando aqui."))
                .asEmpty();

        return ciclistaRepository.save(ciclista);
    }

    public Ciclista alterarCiclista(Long idCiclista, Ciclista ciclista) throws ResourceNotFoundException {
        Ciclista ciclistaDb = ciclistaRepository.findById(idCiclista).orElseThrow(() -> new ResourceNotFoundException("Ciclista não encontrado :: " + idCiclista));

        ciclistaDb.setNome(ciclista.getNome());
        ciclistaDb.setNascimento(ciclista.getNascimento());
        ciclistaDb.setCpf(ciclista.getCpf());
        ciclistaDb.setPassaporte(ciclista.getPassaporte());
        ciclistaDb.setNacionalidade(ciclista.getNacionalidade());
        ciclistaDb.setEmail(ciclista.getEmail());

        ciclistaRepository.save(ciclistaDb);

        return ciclistaDb;
    }

    public String ativarCiclista(Long idCiclista) throws ResourceNotFoundException {
        Ciclista ciclista = ciclistaRepository.findById(idCiclista).orElseThrow(() -> new ResourceNotFoundException("Ciclista não encontrado :: " + idCiclista));

        ciclista.setAtivado(true);
        ciclistaRepository.save(ciclista);

        return "Ciclista ativado";
    }

    public boolean verificaEmail(String email) {
        List<String> emails = ciclistaRepository.findByEmail(email);
        return emails.size() != 0;
    }
}
