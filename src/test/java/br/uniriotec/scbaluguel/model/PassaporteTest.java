package br.uniriotec.scbaluguel.model;

import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public class PassaporteTest {
    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        this.validator = factory.getValidator();
    }

    @Test
    public void naoDeveAceitarFuncionario() {
        Passaporte passaporte = new Passaporte();
        passaporte.setValidade("05/2025");

        Set<ConstraintViolation<Passaporte>> restricoes = validator.validate(passaporte);
        assertThat(restricoes, hasSize(3));
    }

    @Test
    public void deveAceitarFuncionario() {
        Passaporte passaporte = new Passaporte();
        passaporte.setValidade("05/25");
        passaporte.setPais("Brasil");
        passaporte.setNumero("123456");

        Set<ConstraintViolation<Passaporte>> restricoes = validator.validate(passaporte);
        assertThat(restricoes, hasSize(0));
    }
}
