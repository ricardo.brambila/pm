package br.uniriotec.scbaluguel.model;

import lombok.Data;

import javax.persistence.*;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@Entity
@Table(name = "alugueis")
public class Aluguel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "ciclista")
    private Long ciclista;

    @Column(name = "tranca_inicio")
    private String trancaInicio;

    @Column(name = "bicicleta")
    private String bicicleta;

    @Column(name = "hora_inicio")
    private Date horaInicio;

    @Column(name = "tranca_fim")
    private String trancaFim;

    @Column(name = "hora_fim")
    private Date horaFim;

    @Column(name = "cobranca")
    private String cobranca;

    public Aluguel() { }

    public Aluguel(Long ciclista, String trancaInicio, String bicicleta, String trancaFim, String cobranca) {
        this.ciclista = ciclista;
        this.trancaInicio = trancaInicio;
        this.bicicleta = bicicleta;
        this.trancaFim = trancaFim;
        this.cobranca = cobranca;
    }

    public String getHoraInicio() {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return formatter.format(this.horaInicio);
    }

    public String getHoraFim() {
        if(this.horaFim == null) return "";

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return formatter.format(this.horaFim);
    }

    public void setHoraInicio() {
        this.horaInicio = new Date();
    }

    public void setHoraFim() {
        this.horaFim = new Date();
    }
}
