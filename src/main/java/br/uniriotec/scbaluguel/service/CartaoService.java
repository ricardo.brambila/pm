package br.uniriotec.scbaluguel.service;

import br.uniriotec.scbaluguel.exception.ResourceNotFoundException;
import br.uniriotec.scbaluguel.model.Cartao;
import br.uniriotec.scbaluguel.model.Ciclista;
import br.uniriotec.scbaluguel.model.CobrancaResponse;
import br.uniriotec.scbaluguel.repository.ICiclistaRepository;
import kong.unirest.Unirest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CartaoService {
    @Autowired
    private ICiclistaRepository ciclistaRepository;

    public Cartao obterCartao(Long idCiclista) throws ResourceNotFoundException {
        Ciclista ciclista = ciclistaRepository.findById(idCiclista).orElseThrow(() -> new ResourceNotFoundException("Ciclista não encontrado :: " + idCiclista));
        return ciclista.getCartao();
    }

    public Cartao alterarCartao(Long idCiclista, Cartao cartao) throws Exception {
        Ciclista ciclista = ciclistaRepository.findById(idCiclista).orElseThrow(() -> new ResourceNotFoundException("Ciclista não encontrado :: " + idCiclista));
        Cartao cartaoDb = ciclistaRepository.findCardByUserId(idCiclista);

        final CobrancaResponse validaResponse = Unirest
                .post("https://aluguel-de-bicicletas.herokuapp.com/validaCartaoDeCredito")
                .header("Content-Type", "application/json")
                .body(cartao)
                .asObject(CobrancaResponse.class)
                .getBody();

//        if(validaResponse.getCodigo().equals("42X")) throw new Exception(validaResponse.getMensagem());

        cartaoDb.setNomeTitular(cartao.getNomeTitular());
        cartaoDb.setNumero(cartao.getNumero());
        cartaoDb.setCvv(cartao.getCvv());
        cartaoDb.setValidade(cartao.getValidade());

        ciclista.setCartao(cartaoDb);
        ciclistaRepository.save(ciclista);

        return cartaoDb;
    }
}
