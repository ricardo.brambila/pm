package br.uniriotec.scbaluguel.repository;

import br.uniriotec.scbaluguel.model.Aluguel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IAluguelRepository extends JpaRepository<Aluguel, Long> {

    Aluguel findByBicicleta(String idBicicleta);
}
