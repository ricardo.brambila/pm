package br.uniriotec.scbaluguel.model;

import lombok.Data;

@Data
public class CobrancaResponse {
    private String id;
    private String codigo;
    private String mensagem;
}
