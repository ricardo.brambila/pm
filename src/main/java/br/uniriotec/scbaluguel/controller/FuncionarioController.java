package br.uniriotec.scbaluguel.controller;

import br.uniriotec.scbaluguel.exception.ResourceNotFoundException;
import br.uniriotec.scbaluguel.model.Funcionario;
import br.uniriotec.scbaluguel.service.FuncionarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/funcionario")
public class FuncionarioController {
  @Autowired
  private FuncionarioService funcionarioService;

  @GetMapping
  @ResponseBody
  public ResponseEntity<List<Funcionario>> obterFuncionarios() {
    List<Funcionario> funcionarios = this.funcionarioService.obterFuncionarios();
    return ResponseEntity.ok(funcionarios);
  }

  @PostMapping
  @ResponseBody
  public ResponseEntity<Funcionario> cadastrarFuncionario(@RequestBody Funcionario funcionario) {
    Funcionario funcionarioDb = this.funcionarioService.cadastrarFuncionario(funcionario);
    return ResponseEntity.ok(funcionarioDb);
  }

  @GetMapping("/{idFuncionario}")
  @ResponseBody
  public ResponseEntity<Funcionario> obterFuncionario(@PathVariable Long idFuncionario) throws ResourceNotFoundException {
    Funcionario funcionario = this.funcionarioService.obterFuncionario(idFuncionario);

    return ResponseEntity.ok(funcionario);
  }

  @PutMapping("/{idFuncionario}")
  @ResponseBody
  public ResponseEntity<Funcionario> editarFuncionario(@PathVariable Long idFuncionario, @RequestBody Funcionario funcionario) throws ResourceNotFoundException {
    Funcionario funcionarioDb = this.funcionarioService.editarFuncionario(idFuncionario, funcionario);
    return ResponseEntity.ok(funcionarioDb);
  }

  @DeleteMapping("/{idFuncionario}")
  @ResponseBody
  public ResponseEntity<String> removerFuncionario(@PathVariable Long idFuncionario) throws ResourceNotFoundException {
    String msg = this.funcionarioService.removerFuncionario(idFuncionario);
    return ResponseEntity.ok(msg);
  }
}