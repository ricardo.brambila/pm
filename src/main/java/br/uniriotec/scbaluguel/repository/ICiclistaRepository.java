package br.uniriotec.scbaluguel.repository;

import br.uniriotec.scbaluguel.model.Cartao;
import br.uniriotec.scbaluguel.model.Ciclista;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ICiclistaRepository extends JpaRepository<Ciclista, Long> {
    @Query("SELECT email FROM Ciclista c WHERE c.email = :email")
    List<String> findByEmail(String email);

    @Query("SELECT cartao FROM Ciclista c WHERE c.id = :id")
    Cartao findCardByUserId(Long id);
}
