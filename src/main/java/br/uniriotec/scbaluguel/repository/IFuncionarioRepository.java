package br.uniriotec.scbaluguel.repository;

import br.uniriotec.scbaluguel.model.Funcionario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IFuncionarioRepository extends JpaRepository<Funcionario, Long> {
}
