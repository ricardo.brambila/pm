package br.uniriotec.scbaluguel.model;

import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public class CartaoTest {
    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        this.validator = factory.getValidator();
    }

    @Test
    public void naoDeveAceitarCartao() {
        Cartao cartao = new Cartao();
        cartao.setValidade("01/05/2021");
        cartao.setCvv("1234");
        cartao.setNumero("000000");

        Set<ConstraintViolation<Cartao>> restricoes = validator.validate(cartao);
        assertThat(restricoes, hasSize(4));
    }

    @Test
    public void deveAceitarCartao() {
        Cartao cartao = new Cartao();
        cartao.setValidade("05/27");
        cartao.setCvv("123");
        cartao.setNumero("1234567890152348");
        cartao.setNomeTitular("João Silva");

        Set<ConstraintViolation<Cartao>> restricoes = validator.validate(cartao);
        assertThat(restricoes, hasSize(0));
    }
}
