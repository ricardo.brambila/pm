package br.uniriotec.scbaluguel.model;

import lombok.Data;

@Data
public class EnviarEmailRequest {
    private String email;
    private String mensagem;

    public EnviarEmailRequest(String email, String mensagem) {
        this.email = email;
        this.mensagem = mensagem;
    }
}
