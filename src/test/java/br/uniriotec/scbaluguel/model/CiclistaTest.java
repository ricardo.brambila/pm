package br.uniriotec.scbaluguel.model;

import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public class CiclistaTest {
    private Validator validator;

    @Before
    public void setUp() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        this.validator = factory.getValidator();
    }

    @Test
    public void naoDeveAceitarCiclista() {
        Ciclista ciclista = new Ciclista();
        ciclista.setEmail("random@");
        ciclista.setCpf("1234567890");
        ciclista.setNascimento("18/12/19955");

        Set<ConstraintViolation<Ciclista>> restricoes = validator.validate(ciclista);
        assertThat(restricoes, hasSize(6));
    }

    @Test
    public void deveAceitarCiclista() {
        Ciclista ciclista = new Ciclista();
        ciclista.setEmail("joao@gmail.com");
        ciclista.setCpf("09901706084");
        ciclista.setNascimento("18/12/1995");
        ciclista.setNacionalidade("brasileiro");
        ciclista.setNome("João Silva");
        ciclista.setSenha("123");

        Set<ConstraintViolation<Ciclista>> restricoes = validator.validate(ciclista);
        assertThat(restricoes, hasSize(0));
    }
}
